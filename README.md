# Pre-alpha version of RESTful API for transaction flow. 

For `.jar` building call:
```sh
$ mvn clean compile assembly:single
```
That should create file `revolut.jar` in `target` folder. Call buit `.jar` like this:
```sh
$ java -jar revolut.jar {port}
```
where `{port}` is optional param for server indicates which port use to startup. After that, server should be available on `http://localhost:{port}/` if port is available.

**Endpoints:**

`GET /accounts?user_id={user_id}` - get all accouns of the user

`GET /accounts/{account_id}` - get account

`GET /transactions?user_id={user_id}` - get transactions for user

`GET /transactions?account_id={account_id}` - get transactions for account

`POST /transactions/user/user` - create transaction from one user to another user

`POST /transactions/account/user` - create transaction from specific account to another user

`POST /transactions/account/account` - create transaction from one account to another account

All method requires header `content-type:application/json`. `POST` methods gets as body `TransferRequest` object with following structure:

```
{
	"fromId":1,
	"toId":2,
	"currency":"RUB",
	"amount":100
}
```

More precisely objects could be seen in sources.

After server startup there are 2 accounts for `user_id = 1` and 1 account for `user_id = 2`. `transaction` table is empty. 

Some tests available in `test` folder in project sources.