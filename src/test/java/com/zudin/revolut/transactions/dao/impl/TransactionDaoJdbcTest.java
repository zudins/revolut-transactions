package com.zudin.revolut.transactions.dao.impl;

import com.google.inject.Injector;
import com.zudin.revolut.transactions.JettyServer;
import com.zudin.revolut.transactions.model.Account;
import com.zudin.revolut.transactions.model.Currency;
import com.zudin.revolut.transactions.model.Transaction;
import com.zudin.revolut.transactions.model.TransferRequest;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author sergey
 * @since 09.08.17
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionDaoJdbcTest {

    private static TransactionDaoJdbc transactionDaoJdbc;
    private static AccountDaoJdbc accountDaoJdbc;

    @BeforeClass
    public static void setUp() throws Exception {
        Injector injector = JettyServer.getInjector();
        JettyServer.initDBI(injector);
        JettyServer.insertDefault(injector);
        transactionDaoJdbc = injector.getInstance(TransactionDaoJdbc.class);
        accountDaoJdbc = injector.getInstance(AccountDaoJdbc.class);
        accountDaoJdbc.save(Collections.singletonList(new Account(100, Currency.RUB)));
    }

    @Test
    public void test01SaveAndGet() throws Exception {
        Transaction transaction = new Transaction(100, 4, Currency.RUB, new BigDecimal("1.00"));
        transactionDaoJdbc.save(Collections.singletonList(transaction));
        Transaction transaction2 = transactionDaoJdbc.get(1);
        assertNotNull(transaction2);
        assertEquals(transaction.getAmount(), transaction2.getAmount());
        assertEquals(transaction.getCurrency(), transaction2.getCurrency());
        assertEquals(transaction.getUserId(), transaction2.getUserId());
        assertEquals(transaction.getAccountId(), transaction2.getAccountId());
        assertTrue(transaction2.getId() > 0);
        assertNotNull(transaction2.getCreated());
    }

    @Test
    public void test02GetByUser() throws Exception {
        List<Transaction> list = transactionDaoJdbc.getByUser(100);
        checkList(list);
    }

    private void checkList(List<Transaction> list) {
        assertNotNull(list);
        assertEquals(1, list.size());
        Transaction transaction = list.get(0);
        assertNotNull(transaction);
        assertNotNull(transaction.getAmount());
        assertNotNull(transaction.getCreated());
        assertNotNull(transaction.getCurrency());
        assertEquals(4, transaction.getAccountId());
    }

    @Test
    public void test03GetByAccount() throws Exception {
        List<Transaction> list = transactionDaoJdbc.getByAccount(4);
        checkList(list);
    }

    @Test
    public void test04TransferA2AOk() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1.00"), Currency.EUR);
        transactionDaoJdbc.transferA2A(request);
        List<Transaction> list = transactionDaoJdbc.getByAccount(request.getFromId());
        assertEquals(1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        list = transactionDaoJdbc.getByAccount(request.getToId());
        assertEquals(1, list.size());
        transaction = list.get(0);
        assertEquals(request.getToId(), transaction.getAccountId());
        assertEquals(request.getAmount(), transaction.getAmount());
        assertEquals(request.getCurrency(), transaction.getCurrency());
    }

    @Test
    public void test05TransferA2AFail1() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1000"), Currency.EUR);
        Account account1 = accountDaoJdbc.get(request.getFromId());
        Account account2 = accountDaoJdbc.get(request.getToId());
        int size1 = transactionDaoJdbc.getByAccount(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByAccount(request.getToId()).size();
        try {
            transactionDaoJdbc.transferA2A(request);
            assertTrue(false);
        } catch (IllegalStateException e) {
            int size1After = transactionDaoJdbc.getByAccount(request.getFromId()).size();
            assertEquals(size1, size1After);
            int size2After = transactionDaoJdbc.getByAccount(request.getToId()).size();
            assertEquals(size2, size2After);
            Account afterAccount1 = accountDaoJdbc.get(1);
            assertEquals(account1.getBalance(), afterAccount1.getBalance());
            Account afterAccount2 = accountDaoJdbc.get(2);
            assertEquals(account2.getBalance(), afterAccount2.getBalance());
        }
    }

    @Test
    public void test06TransferA2AFail2() throws Exception {
        TransferRequest request = new TransferRequest(1, 5, new BigDecimal("1000"), Currency.EUR);
        Account account1 = accountDaoJdbc.get(request.getFromId());
        int size1 = transactionDaoJdbc.getByAccount(request.getFromId()).size();
        try {
            transactionDaoJdbc.transferA2A(request);
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            int size1After = transactionDaoJdbc.getByAccount(request.getFromId()).size();
            assertEquals(size1, size1After);
            assertTrue(transactionDaoJdbc.getByAccount(request.getToId()).isEmpty());
            Account afterAccount1 = accountDaoJdbc.get(1);
            assertEquals(account1.getBalance(), afterAccount1.getBalance());
        }
    }

    @Test
    public void test07TransferA2UOk1() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1.00"), Currency.RUB);
        int size1 = transactionDaoJdbc.getByAccount(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        transactionDaoJdbc.transferA2U(request);
        List<Transaction> list = transactionDaoJdbc.getByAccount(request.getFromId());
        assertEquals(size1 + 1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        list = transactionDaoJdbc.getByUser(request.getToId());
        assertEquals(size2 + 1, list.size());
        boolean condition = false;
        for (Transaction t : list) {
            if (t.getAmount().equals(request.getAmount()) && request.getCurrency().equals(t.getCurrency())) {
                condition = true;
            }
        }
        assertTrue(condition);
    }

    @Test
    public void test08TransferA2UOk2() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1.00"), Currency.EUR);
        Account toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNull(toAccount);
        int size1 = transactionDaoJdbc.getByAccount(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        transactionDaoJdbc.transferA2U(request);
        List<Transaction> list = transactionDaoJdbc.getByAccount(request.getFromId());
        assertEquals(size1 + 1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNotNull(toAccount);
        list = transactionDaoJdbc.getByUser(request.getToId());
        assertEquals(size2 + 1, list.size());
        boolean condition = false;
        for (Transaction t : list) {
            if (t.getAmount().equals(request.getAmount()) && request.getCurrency().equals(t.getCurrency())) {
                condition = true;
            }
        }
        assertTrue(condition);
    }

    @Test
    public void test09TransferA2UOk3() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("2.00"), Currency.EUR);
        Account toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNotNull(toAccount);
        toAccount.setStatus(Account.Status.DISABLED);
        accountDaoJdbc.update(Collections.singletonList(toAccount));
        int size1 = transactionDaoJdbc.getByAccount(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        transactionDaoJdbc.transferA2U(request);
        List<Transaction> list = transactionDaoJdbc.getByAccount(request.getFromId());
        assertEquals(size1 + 1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNotNull(toAccount);
        assertEquals(Account.Status.ACTIVE, toAccount.getStatus());
        list = transactionDaoJdbc.getByUser(request.getToId());
        assertEquals(size2 + 1, list.size());
        boolean condition = false;
        for (Transaction t : list) {
            if (t.getAmount().equals(request.getAmount()) && request.getCurrency().equals(t.getCurrency())) {
                condition = true;
            }
        }
        assertTrue(condition);
    }

    @Test
    public void test10TransferA2UFail1() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1000"), Currency.EUR);
        Account account1 = accountDaoJdbc.get(request.getFromId());
        Account account2 = accountDaoJdbc.get(request.getToId());
        int size1 = transactionDaoJdbc.getByAccount(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        try {
            transactionDaoJdbc.transferA2U(request);
            assertTrue(false);
        } catch (IllegalStateException e) {
            int size1After = transactionDaoJdbc.getByAccount(request.getFromId()).size();
            assertEquals(size1, size1After);
            int size2After = transactionDaoJdbc.getByUser(request.getToId()).size();
            assertEquals(size2, size2After);
            Account afterAccount1 = accountDaoJdbc.get(1);
            assertEquals(account1.getBalance(), afterAccount1.getBalance());
            Account afterAccount2 = accountDaoJdbc.get(2);
            assertEquals(account2.getBalance(), afterAccount2.getBalance());
        }
    }

    @Test
    public void test11TransferU2UOk1() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1.00"), Currency.RUB);
        int size1 = transactionDaoJdbc.getByUser(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        transactionDaoJdbc.transferA2U(request);
        List<Transaction> list = transactionDaoJdbc.getByUser(request.getFromId());
        assertEquals(size1 + 1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        list = transactionDaoJdbc.getByUser(request.getToId());
        assertEquals(size2 + 1, list.size());
        boolean condition = false;
        for (Transaction t : list) {
            if (t.getAmount().equals(request.getAmount()) && request.getCurrency().equals(t.getCurrency())) {
                condition = true;
            }
        }
        assertTrue(condition);
    }

    @Test
    public void test12TransferU2UOk2() throws Exception {
        accountDaoJdbc.save(Collections.singletonList(new Account(1, Currency.GBP)));

        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1.00"), Currency.GBP);
        Account toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNull(toAccount);
        int size1 = transactionDaoJdbc.getByUser(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        transactionDaoJdbc.transferA2U(request);
        List<Transaction> list = transactionDaoJdbc.getByUser(request.getFromId());
        assertEquals(size1 + 1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNotNull(toAccount);
        list = transactionDaoJdbc.getByUser(request.getToId());
        assertEquals(size2 + 1, list.size());
        boolean condition = false;
        for (Transaction t : list) {
            if (t.getAmount().equals(request.getAmount()) && request.getCurrency().equals(t.getCurrency())) {
                condition = true;
            }
        }
        assertTrue(condition);
    }

    @Test
    public void test13TransferU2UOk3() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("2.00"), Currency.EUR);
        Account toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNotNull(toAccount);
        toAccount.setStatus(Account.Status.DISABLED);
        accountDaoJdbc.update(Collections.singletonList(toAccount));
        int size1 = transactionDaoJdbc.getByUser(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        transactionDaoJdbc.transferA2U(request);
        List<Transaction> list = transactionDaoJdbc.getByUser(request.getFromId());
        assertEquals(size1 + 1, list.size());
        Transaction transaction = list.get(0);
        assertEquals(request.getFromId(), transaction.getAccountId());

        toAccount = accountDaoJdbc.getByUser(request.getToId(), request.getCurrency());
        assertNotNull(toAccount);
        assertEquals(Account.Status.ACTIVE, toAccount.getStatus());
        list = transactionDaoJdbc.getByUser(request.getToId());
        assertEquals(size2 + 1, list.size());
        boolean condition = false;
        for (Transaction t : list) {
            if (t.getAmount().equals(request.getAmount()) && request.getCurrency().equals(t.getCurrency())) {
                condition = true;
            }
        }
        assertTrue(condition);
    }

    @Test
    public void test14TransferA2UFail1() throws Exception {
        TransferRequest request = new TransferRequest(1, 2, new BigDecimal("1000"), Currency.EUR);
        int size1 = transactionDaoJdbc.getByUser(request.getFromId()).size();
        int size2 = transactionDaoJdbc.getByUser(request.getToId()).size();
        try {
            transactionDaoJdbc.transferU2U(request);
            assertTrue(false);
        } catch (IllegalStateException e) {
            int size1After = transactionDaoJdbc.getByUser(request.getFromId()).size();
            assertEquals(size1, size1After);
            int size2After = transactionDaoJdbc.getByUser(request.getToId()).size();
            assertEquals(size2, size2After);
        }
    }

    @Test
    public void test15TransferA2UFail2() throws Exception {
        TransferRequest request = new TransferRequest(2, 1, new BigDecimal("1"), Currency.USD);
        Account account = accountDaoJdbc.getByUser(request.getFromId(), request.getCurrency());
        assertNull(account);
        int size1 = transactionDaoJdbc.getByUser(request.getFromId()).size();
        try {
            transactionDaoJdbc.transferU2U(request);
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            int size1After = transactionDaoJdbc.getByUser(request.getFromId()).size();
            assertEquals(size1, size1After);
        }
    }


}