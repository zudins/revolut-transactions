package com.zudin.revolut.transactions.dao.impl;

import com.google.inject.Injector;
import com.zudin.revolut.transactions.JettyServer;
import com.zudin.revolut.transactions.model.Account;
import com.zudin.revolut.transactions.model.Currency;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author sergey
 * @since 09.08.17
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountDaoJdbiTest {

    private static AccountDaoJdbc accountDaoJdbc;

    @BeforeClass
    public static void setUp() throws Exception {
        Injector injector = JettyServer.getInjector();
        JettyServer.initDBI(injector);
        accountDaoJdbc = injector.getInstance(AccountDaoJdbc.class);
    }

    @Test
    public void test1SaveAndGet() throws Exception {
        accountDaoJdbc.save(Collections.singletonList(new Account(1, Currency.RUB)));
        accountDaoJdbc.save(Collections.singletonList(new Account(1, Currency.EUR)));
        Account account = accountDaoJdbc.get(1);
        checkAccount1(account);
    }

    private void checkAccount1(Account account) {
        assertNotNull(account);
        assertEquals(1, account.getId());
        assertEquals(1, account.getUserId());
        assertEquals(Currency.RUB, account.getCurrency());
        assertNotNull(account.getCreated());
        assertNotNull(account.getBalance());
        assertTrue(account.getBalance().compareTo(BigDecimal.ZERO) >= 0);
        assertNotNull(account.getChanged());
    }

    @Test
    public void test2GetByUser1() throws Exception {
        List<Account> accounts = accountDaoJdbc.getByUser(1);
        assertNotNull(accounts);
        assertEquals(2, accounts.size());
        for (Account account : accounts) {
            assertTrue(account.getId() > 0);
            if (account.getId() == 1) {
                checkAccount1(account);
            }
        }
    }

    @Test
    public void test3GetByUser() throws Exception {
        checkAccount1(accountDaoJdbc.getByUser(1, Currency.RUB));
    }

    @Test
    public void test5Update() throws Exception {
        Account account = accountDaoJdbc.get(1);
        account.setStatus(Account.Status.DISABLED);
        account.addBalance(new BigDecimal("100"));
        account.setId(1);
        account.setUserId(100);
        BigDecimal balance = account.getBalance();
        accountDaoJdbc.update(Collections.singletonList(account));
        account = accountDaoJdbc.get(1);
        assertNotNull(account);
        assertEquals(1, account.getUserId());
        assertEquals(balance, account.getBalance());
    }
}