package com.zudin.revolut.transactions.util;

import com.zudin.revolut.transactions.model.Currency;
import yahoofinance.YahooFinance;
import yahoofinance.quotes.fx.FxQuote;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author sergey
 * @since 08.08.17
 */
public class CurrencyConverter {

    public BigDecimal convert(BigDecimal amount, Currency from, Currency to) throws IOException {
        FxQuote rate = YahooFinance.getFx(from.name() + to.name() + "=X");
        return rate.getPrice().multiply(amount);
    }

    public static void main(String[] args) throws IOException {
        CurrencyConverter currencyConverter = new CurrencyConverter();
        BigDecimal convert = currencyConverter.convert(new BigDecimal(100), Currency.GBP, Currency.RUB);
        System.out.println(convert);
    }

}
