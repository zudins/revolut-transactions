package com.zudin.revolut.transactions.model;

/**
 * @author sergey
 * @since 08.08.17
 */
public enum Currency {

    RUB,
    EUR,
    USD,
    GBP

}
