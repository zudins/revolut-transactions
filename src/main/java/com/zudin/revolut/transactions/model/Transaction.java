package com.zudin.revolut.transactions.model;

import com.github.rkmk.annotations.PrimaryKey;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sergey
 * @since 08.08.17
 */
public class Transaction {

    @PrimaryKey
    private int id;
    private int userId;
    private int accountId;
    private Integer transactionGroupId;
    private Currency currency;
    private BigDecimal amount;
    private Date created;

    public Transaction() {
    }

    public Transaction(int userId, int accountId, Currency currency, BigDecimal amount) {
        this.userId = userId;
        this.accountId = accountId;
        this.currency = currency;
        this.amount = amount;
    }

    public Transaction(int userId, int accountId, int transactionGroupId, Currency currency, BigDecimal amount) {
        this.userId = userId;
        this.accountId = accountId;
        this.transactionGroupId = transactionGroupId;
        this.currency = currency;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public Integer getTransactionGroupId() {
        return transactionGroupId;
    }

    public void setTransactionGroupId(int transactionGroupId) {
        this.transactionGroupId = transactionGroupId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
