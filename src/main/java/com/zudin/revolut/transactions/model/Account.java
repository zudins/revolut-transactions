package com.zudin.revolut.transactions.model;

import com.github.rkmk.annotations.PrimaryKey;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sergey
 * @since 08.08.17
 */
public class Account {

    @PrimaryKey
    private int id;
    private int userId;
    private Currency currency;
    private BigDecimal balance;
    private Status status;
    private Date created;
    private Date changed;

    public Account() {
    }

    public Account(int userId, Currency currency) {
        this.userId = userId;
        this.currency = currency;
    }

    public Account(int id, int userId, Currency currency, BigDecimal balance) {
        this.id = id;
        this.userId = userId;
        this.currency = currency;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void addBalance(BigDecimal delta) {
        this.balance = this.balance.add(delta);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public enum Status {
        ACTIVE,
        DISABLED
    }

}
