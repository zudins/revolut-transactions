package com.zudin.revolut.transactions.model;

import java.math.BigDecimal;

/**
 * @author sergey
 * @since 08.08.17
 */
public class TransferRequest {

    private int fromId;
    private int toId;
    private BigDecimal amount;
    private Currency currency;

    public TransferRequest() {
    }

    public TransferRequest(int fromId, int toId, BigDecimal amount, Currency currency) {
        this.fromId = fromId;
        this.toId = toId;
        this.amount = amount;
        this.currency = currency;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
