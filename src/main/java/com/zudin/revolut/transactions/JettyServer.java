package com.zudin.revolut.transactions;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceFilter;
import com.zudin.revolut.transactions.dao.impl.AccountDaoJdbc;
import com.zudin.revolut.transactions.model.Account;
import com.zudin.revolut.transactions.model.Currency;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;

/**
 * @author sergey
 * @since 05.08.17
 */
public class JettyServer {

    public static void main(String[] args) throws Exception {
        Injector injector = getInjector();
        initDBI(injector); //for testing
        insertDefault(injector);
        startServer(args, injector);
    }

    public static Injector getInjector() {
        ApplicationModule applicationModule = new ApplicationModule();
        return Guice.createInjector(applicationModule);
    }

    public static void initDBI(Injector injector) {
        DBI dbi = injector.getInstance(DBI.class);
        dbi.withHandle(h -> {
            h.execute("create table `account` (" +
                    "`id` int(11) not null auto_increment, " +
                    "`user_id` int(11) not null," +
                    "`currency` varchar(5) not null," +
                    "`balance` decimal(10,2) not null default '0.00', " +
                    "`status` varchar(32) not null default 'ACTIVE'," +
                    "`created` timestamp not null default NOW()," +
                    "`changed` timestamp not null default NOW()," +
                    "PRIMARY KEY (`id`), KEY `acc_user_id` (`user_id`))");

            h.execute("create table `transaction` (" +
                    "`id` int(11) not null auto_increment, " +
                    "`user_id` int(11) not null," +
                    "`account_id` int(11) not null," +
                    "`transaction_group_id` int(11) not null," +
                    "`currency` varchar(5) not null," +
                    "`amount` decimal(10,2) not null default '0.00', " +
                    "`created` timestamp not null default NOW()," +
                    "PRIMARY KEY (`id`), " +
                    "KEY `tr_user_id` (`user_id`)," +
                    "KEY `tr_account_id` (`account_id`)," +
                    "KEY `tr_transaction_group_id` (`transaction_group_id`))");

            return null;
        });
    }

    public static void insertDefault(Injector injector) {
        AccountDaoJdbc accountDao = injector.getInstance(AccountDaoJdbc.class);
        accountDao.save(Collections.singletonList(new Account(1, Currency.RUB)));
        accountDao.save(Collections.singletonList(new Account(1, Currency.EUR)));
        accountDao.save(Collections.singletonList(new Account(2, Currency.RUB)));
        Account account1 = accountDao.get(1);
        account1.addBalance(new BigDecimal("1000.00"));
        Account account2 = accountDao.get(2);
        account2.addBalance(new BigDecimal("10.00"));
        accountDao.update(Arrays.asList(account1, account2));
    }

    private static void startServer(String[] args, Injector injector) throws Exception {
        int port = 8080;
        if (args.length > 0) {
            String newPort = args[0];
            try {
                port = Integer.parseInt(newPort);
            } catch (Exception e) {
                System.err.println("Wrong format for port: " + newPort);
            }
        }
        Server server = new Server(port);

        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));

        servletContextHandler.addServlet(HttpServletDispatcher.class, "/");
        servletContextHandler.addEventListener(new GuiceResteasyBootstrapServletContextListener());
        servletContextHandler.setInitParameter("resteasy.guice.modules", "com.zudin.revolut.transactions.ApplicationModule");
        servletContextHandler.setInitParameter("resteasy.scan", "true");

        server.start();
        System.out.println("Server is started on http://localhost:"+port);
        server.join();
    }

}
