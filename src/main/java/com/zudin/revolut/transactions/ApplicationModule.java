package com.zudin.revolut.transactions;

import com.github.rkmk.mapper.CustomMapperFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.zudin.revolut.transactions.web.AccountController;
import com.zudin.revolut.transactions.web.JacksonConfig;
import com.zudin.revolut.transactions.web.TransactionController;
import org.jboss.resteasy.plugins.providers.jackson.ResteasyJacksonProvider;
import org.skife.jdbi.v2.DBI;

/**
 * @author sergey
 * @since 05.08.17
 */
public class ApplicationModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ResteasyJacksonProvider.class);
        bind(JacksonConfig.class);
        bind(TransactionController.class);
        bind(AccountController.class);
    }

    @Provides
    public DBI provideTransactionLog() {
        new org.h2.Driver();
        DBI dbi = new DBI("jdbc:h2:mem:test;MODE=MySQL;DATABASE_TO_UPPER=false;DB_CLOSE_DELAY=-1");
        dbi.registerMapper(new CustomMapperFactory());
        return dbi;
    }

}
