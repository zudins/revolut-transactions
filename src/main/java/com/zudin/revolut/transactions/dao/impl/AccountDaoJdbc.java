package com.zudin.revolut.transactions.dao.impl;

import com.google.inject.Inject;
import com.zudin.revolut.transactions.dao.AccountDao;
import com.zudin.revolut.transactions.model.Account;
import com.zudin.revolut.transactions.model.Currency;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.sqlobject.*;

import java.util.Collection;
import java.util.List;

/**
 * @author sergey
 * @since 08.08.17
 */
public class AccountDaoJdbc implements AccountDao {

    private final DBI dbi;
    private AccountDaoJdbi accountDaoJdbi;

    @Inject
    public AccountDaoJdbc(DBI dbi) {
        this.dbi = dbi;
        this.accountDaoJdbi = dbi.onDemand(AccountDaoJdbi.class);
    }

    @Transaction
    public void save(Collection<Account> accounts) {
        accountDaoJdbi.save(accounts);
    }

    public List<Account> getByUser(int userId) {
        return accountDaoJdbi.getByUser(userId);
    }

    public Account getByUser(int userId, Currency currency) {
        return accountDaoJdbi.getByUser(userId, currency);
    }

    public Account get(int id) {
        return accountDaoJdbi.get(id);
    }

    //    @Transaction
    public void update(Collection<Account> accounts) {
        accountDaoJdbi.update(accounts);
    }

    protected abstract static class AccountDaoJdbi implements AccountDao {

        public AccountDaoJdbi() {
        }

        @SqlBatch("insert into account (user_id, currency) values (:userId, :currency)")
        public abstract void save(@BindBean Collection<Account> accounts);

        @SqlQuery("select * from account where id = :id")
        public abstract Account get(@Bind("id") int id);

        @SqlQuery("select * from account where user_id = :userId and currency = :currency")
        public abstract Account getByUser(@Bind("userId") int userId, @Bind("currency") Currency currency);

        @SqlQuery("select * from account where user_id = :userId")
        public abstract List<Account> getByUser(@Bind("userId") int userId);

        @SqlBatch("update account set status = :status, balance = :balance where id = :id")
        public abstract void update(@BindBean Collection<Account> accounts);

    }

}
