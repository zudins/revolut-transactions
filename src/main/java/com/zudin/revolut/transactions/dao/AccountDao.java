package com.zudin.revolut.transactions.dao;

import com.google.inject.ImplementedBy;
import com.zudin.revolut.transactions.dao.impl.AccountDaoJdbc;
import com.zudin.revolut.transactions.model.Account;
import com.zudin.revolut.transactions.model.Currency;

import java.util.Collection;
import java.util.List;

/**
 * @author sergey
 * @since 08.08.17
 */
@ImplementedBy(AccountDaoJdbc.class)
public interface AccountDao {

    void save(Collection<Account> accounts);

    List<Account> getByUser(int userId);

    Account getByUser(int userId, Currency currency);

    Account get(int accountId);

    void update(Collection<Account> accounts);

}
