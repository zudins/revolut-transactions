package com.zudin.revolut.transactions.dao;

import com.google.inject.ImplementedBy;
import com.zudin.revolut.transactions.dao.impl.TransactionDaoJdbc;
import com.zudin.revolut.transactions.model.Transaction;
import com.zudin.revolut.transactions.model.TransferRequest;

import java.util.Collection;
import java.util.List;

/**
 * @author sergey
 * @since 05.08.17
 */
@ImplementedBy(TransactionDaoJdbc.class)
public interface TransactionDao {

    void transferU2U(TransferRequest transferRequest);

    void transferA2U(TransferRequest transferRequest);

    void transferA2A(TransferRequest transferRequest);

    void save(Collection<Transaction> transactions);

    List<Transaction> getByUser(int userId);

    List<Transaction> getByAccount(int accountId);

    Transaction get(int id);

}
