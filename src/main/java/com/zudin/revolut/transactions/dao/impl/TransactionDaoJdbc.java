package com.zudin.revolut.transactions.dao.impl;

import com.google.inject.Inject;
import com.zudin.revolut.transactions.dao.TransactionDao;
import com.zudin.revolut.transactions.model.Account;
import com.zudin.revolut.transactions.model.Transaction;
import com.zudin.revolut.transactions.model.TransferRequest;
import com.zudin.revolut.transactions.util.CurrencyConverter;
import org.apache.commons.lang3.tuple.Pair;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.TransactionIsolationLevel;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlBatch;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * @author sergey
 * @since 05.08.17
 */
public class TransactionDaoJdbc implements TransactionDao {

    private final DBI dbi;
    private final CurrencyConverter currencyConverter;
    private TransactionDaoJdbi transactionDaoJdbi;

    @Inject
    public TransactionDaoJdbc(DBI dbi, CurrencyConverter currencyConverter) {
        this.dbi = dbi;
        this.currencyConverter = currencyConverter;
        this.transactionDaoJdbi = dbi.onDemand(TransactionDaoJdbi.class);
    }

    @Override
    @org.skife.jdbi.v2.sqlobject.Transaction(TransactionIsolationLevel.READ_UNCOMMITTED)
    public void transferU2U(TransferRequest transferRequest) {
        createTransactions(transferRequest, (accountDaoJdbi) -> {
            Account fromAccount = accountDaoJdbi.getByUser(transferRequest.getFromId(), transferRequest.getCurrency());
            Account toAccount = getOrCreateAccount(transferRequest, accountDaoJdbi);
            return Pair.of(fromAccount, toAccount);
        });
    }

    @Override
    @org.skife.jdbi.v2.sqlobject.Transaction(TransactionIsolationLevel.READ_UNCOMMITTED)
    public void transferA2U(TransferRequest transferRequest) {
        createTransactions(transferRequest, (accountDaoJdbi) -> {
            Account fromAccount = accountDaoJdbi.get(transferRequest.getFromId());
            Account toAccount = getOrCreateAccount(transferRequest, accountDaoJdbi);
            return Pair.of(fromAccount, toAccount);
        });
    }

    private Account getOrCreateAccount(TransferRequest transferRequest, AccountDaoJdbc.AccountDaoJdbi accountDaoJdbi) {
        Account toAccount = accountDaoJdbi.getByUser(transferRequest.getToId(), transferRequest.getCurrency());
        if (toAccount == null) {
            accountDaoJdbi.save(Collections.singletonList(new Account(transferRequest.getToId(), transferRequest.getCurrency())));
            toAccount = accountDaoJdbi.getByUser(transferRequest.getToId(), transferRequest.getCurrency());
        } else {
            toAccount.setStatus(Account.Status.ACTIVE);
        }
        return toAccount;
    }

    @Override
    @org.skife.jdbi.v2.sqlobject.Transaction(TransactionIsolationLevel.READ_UNCOMMITTED)
    public void transferA2A(TransferRequest transferRequest) {
        createTransactions(transferRequest, (accountDaoJdbi) -> {
            Account fromAccount = accountDaoJdbi.get(transferRequest.getFromId());
            Account toAccount = accountDaoJdbi.get(transferRequest.getToId());
            return Pair.of(fromAccount, toAccount);
        });
    }

    private void createTransactions(TransferRequest transferRequest,
                                    Function<AccountDaoJdbc.AccountDaoJdbi, Pair<Account, Account>> function) {
        try {
            Handle handle =  dbi.open();
            AccountDaoJdbc.AccountDaoJdbi accountDaoJdbi = handle.attach(AccountDaoJdbc.AccountDaoJdbi.class);
            TransactionDaoJdbi transactionDaoJdbi = handle.attach(TransactionDaoJdbi.class);
            handle.begin();

            Pair<Account, Account> accountPair = function.apply(accountDaoJdbi);
            Account fromAccount = accountPair.getLeft();
            Account toAccount = accountPair.getRight();

            if (fromAccount == null || toAccount == null) {
                throw new IllegalArgumentException("Cannot find appropriate accounts");
            }

            BigDecimal fromTransactionAmount = currencyConverter.convert(transferRequest.getAmount(),
                    transferRequest.getCurrency(), fromAccount.getCurrency()).negate();
            if (fromAccount.getBalance().compareTo(fromTransactionAmount.abs()) < 0) {
                throw new IllegalStateException("Too low balance");
            }
            BigDecimal toTransactionAmount = currencyConverter.convert(transferRequest.getAmount(),
                    transferRequest.getCurrency(), toAccount.getCurrency());
            Integer groupId = handle.createQuery("select max(transaction_group_id) from transaction")
                    .mapTo(Integer.class)
                    .first();
            if (groupId == null) {
                groupId = 10;
            } else {
                groupId++;
            }
            Transaction fromTransaction = new Transaction(fromAccount.getUserId(), fromAccount.getId(),
                    groupId, fromAccount.getCurrency(), fromTransactionAmount);
            Transaction toTransaction = new Transaction(toAccount.getUserId(), toAccount.getId(),
                    groupId, toAccount.getCurrency(), toTransactionAmount);
            fromAccount.addBalance(fromTransactionAmount);
            toAccount.addBalance(toTransactionAmount);
            transactionDaoJdbi.save(Arrays.asList(fromTransaction, toTransaction));
            accountDaoJdbi.update(Arrays.asList(fromAccount, toAccount));

            handle.commit();
            handle.close();
        } catch (IOException e) {
            throw new InternalError("Cannot process transfer", e);
        }
    }

    @Override
    public void save(Collection<Transaction> transactions) {
        transactionDaoJdbi.save(transactions);
    }

    @Override
    public List<Transaction> getByUser(int userId) {
        return transactionDaoJdbi.getByUser(userId);
    }

    @Override
    public List<Transaction> getByAccount(int accountId) {
        return transactionDaoJdbi.getByAccount(accountId);
    }

    @Override
    public Transaction get(int id) {
        return transactionDaoJdbi.get(id);
    }

    private abstract static class TransactionDaoJdbi implements TransactionDao {

        public TransactionDaoJdbi() {
        }

        @Override
        @SqlBatch("insert into transaction (user_id, account_id, transaction_group_id, currency, amount) " +
                "values (:userId, :accountId, :transactionGroupId, :currency, :amount)")
        public abstract void save(@BindBean Collection<Transaction> transactions);

        @Override
        @SqlQuery("select * from transaction where user_id = :userId")
        public abstract List<Transaction> getByUser(@Bind("userId") int userId);

        @Override
        @SqlQuery("select * from transaction where account_id = :accountId")
        public abstract List<Transaction> getByAccount(@Bind("accountId") int accountId);

        @Override
        @SqlQuery("select * from transaction where id = :id")
        public abstract Transaction get(@Bind("id") int id);

    }


}
