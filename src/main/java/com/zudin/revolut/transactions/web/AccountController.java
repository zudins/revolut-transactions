package com.zudin.revolut.transactions.web;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.zudin.revolut.transactions.dao.AccountDao;
import com.zudin.revolut.transactions.model.Account;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author sergey
 * @since 05.08.17
 */

@Singleton
@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountController {

    private final AccountDao accountDao;

    @Inject
    public AccountController(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @GET
    public Response get(@QueryParam("user_id") Integer userId) {
        if (userId != null && userId > 0) {
            return Response.ok(accountDao.getByUser(userId)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(ImmutableMap.of("success", false, "error", "Appropriate id must be present"))
                .build();
    }

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int accountId) {
        if (accountId > 0) {
            Account account = accountDao.get(accountId);
            if (account == null) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity(ImmutableMap.of("success", false, "error", "Cannot find account with given id"))
                        .build();
            }
            return Response.ok(account).build();
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(ImmutableMap.of("success", false, "error", "Appropriate id must be present"))
                .build();
    }

}
