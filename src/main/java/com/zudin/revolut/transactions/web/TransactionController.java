package com.zudin.revolut.transactions.web;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.zudin.revolut.transactions.dao.TransactionDao;
import com.zudin.revolut.transactions.model.TransferRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.function.Consumer;

/**
 * @author sergey
 * @since 05.08.17
 */

@Singleton
@Path("/transactions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransactionController {

    private final TransactionDao transactionDao;

    @Inject
    public TransactionController(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    @GET
    public Response get(@QueryParam("user_id") Integer userId, @QueryParam("account_id") Integer accountId) {
        if (userId != null && userId > 0) {
            return Response.ok(transactionDao.getByUser(userId)).build();
        } else if (accountId != null && accountId > 0) {
            return Response.ok(transactionDao.getByAccount(accountId)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(ImmutableMap.of("success", false, "error", "Appropriate id must be present"))
                .build();
    }

    @POST
    @Path("/user/user")
    public Response userToUser(TransferRequest transferRequest) throws IOException {
        return getResponse(transferRequest, transactionDao::transferU2U);
    }

    @POST
    @Path("/account/user")
    public Response accountToUser(TransferRequest transferRequest) throws IOException {
        return getResponse(transferRequest, transactionDao::transferA2U);
    }

    @POST
    @Path("/account/account")
    public Response accountToAccount(TransferRequest transferRequest) throws IOException {
        return getResponse(transferRequest, transactionDao::transferA2A);
    }

    private Response getResponse(TransferRequest transferRequest, Consumer<TransferRequest> requestConsumer) {
        try {
            requestConsumer.accept(transferRequest);
            return Response.ok(ImmutableMap.of("success", true)).build();
        } catch (IllegalStateException e) {
            return Response.status(Response.Status.PRECONDITION_FAILED)
                    .entity(ImmutableMap.of("success", false, "error", e.getMessage()))
                    .build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(ImmutableMap.of("success", false, "error", e.getMessage()))
                    .build();
        } catch (Throwable t) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
